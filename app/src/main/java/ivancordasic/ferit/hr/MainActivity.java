package ivancordasic.ferit.hr;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    Button btnInspiration;
    Button btnDescription;

    RadioGroup rgPersonSelect;

    RadioButton rbFirstPerson;
    RadioButton rbSecondPerson;
    RadioButton rbThirdPerson;

    EditText etEditDescription;

    TextView tvNameNikolaTesla;
    TextView tvNameLewisHamilton;
    TextView tvNameTomislavPavlicic;

    TextView tvYearNikolaTesla;
    TextView tvYearLewisHamilton;
    TextView tvYearTomislavPavlicic;

    TextView tvDescriptionNikolaTesla;
    TextView tvDescriptionLewisHamilton;
    TextView tvDescriptionTomislavPavlicic;

    TextView tvTop_3;

    ImageView ivNikolaTesla;
    ImageView ivLewisHamilton;
    ImageView ivTomislavPavlicic;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }
    private void initializeUI() {
        this.btnInspiration = (Button) findViewById(R.id.btnInspiration);
        this.btnDescription = (Button) findViewById(R.id.btnEditDescription);

        this.rgPersonSelect = (RadioGroup) findViewById(R.id.rgPersonSelect);

        this.rbFirstPerson = (RadioButton) findViewById(R.id.rbFirstPerson);
        this.rbSecondPerson = (RadioButton) findViewById(R.id.rbSecondPerson);
        this.rbThirdPerson = (RadioButton) findViewById(R.id.rbThirdPerson);

        this.etEditDescription = (EditText) findViewById(R.id.etDescription);

        this.tvNameNikolaTesla = (TextView) findViewById(R.id.tvNameNikolaTesla);
        this.tvNameLewisHamilton = (TextView) findViewById(R.id.tvLewisHamilton);
        this.tvNameTomislavPavlicic = (TextView) findViewById(R.id.tvNameTomislavPavlicic);

        this.tvYearNikolaTesla = (TextView) findViewById(R.id.tvYearNikolaTesla);
        this.tvYearLewisHamilton = (TextView) findViewById(R.id.tvYearLewisHamilton);
        this.tvYearTomislavPavlicic = (TextView) findViewById(R.id.tvYearTomislavPavlicic);

        this.tvDescriptionNikolaTesla = (TextView) findViewById(R.id.tvDescriptionNikolaTesla);
        this.tvDescriptionLewisHamilton = (TextView) findViewById(R.id.tvDescriptionLewisHamilton);
        this.tvDescriptionTomislavPavlicic = (TextView) findViewById(R.id.tvDescriptionTomislavPavlicic);

        this.tvTop_3 = (TextView) findViewById(R.id.tvTop3);

        this.ivNikolaTesla = (ImageView) findViewById(R.id.ivNikolaTesla);
        this.ivLewisHamilton = (ImageView) findViewById(R.id.ivLewisHamilton);
        this.ivTomislavPavlicic = (ImageView) findViewById(R.id.ivTomislavPavlicic);

        View.OnClickListener ivListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePicture(v);
            }
        };
        ivNikolaTesla.setOnClickListener(ivListener);
        ivLewisHamilton.setOnClickListener(ivListener);
        ivTomislavPavlicic.setOnClickListener(ivListener);

        View.OnClickListener inspirationListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources resources = getResources();
                String[] quotes = resources.getStringArray(R.array.saQuotes);
                displayToat(quotes[random(quotes.length)]);
            }
        };
        btnInspiration.setOnClickListener(inspirationListener);
        View.OnClickListener descriptionListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   if(rbFirstPerson.isChecked()) {
                       tvDescriptionNikolaTesla.setText(etEditDescription.getText().toString());
                   } else if (rbSecondPerson.isChecked()) {
                       tvDescriptionLewisHamilton.setText(etEditDescription.getText().toString());
                   } else {
                       tvDescriptionTomislavPavlicic.setText(etEditDescription.getText().toString());
                   }
            }
        };
        btnDescription.setOnClickListener(descriptionListener);
    }

    private void deletePicture(View v) {
        v.setVisibility(View.INVISIBLE);
    }
    private int random(int limit) {
        Random random = new Random();
        return random.nextInt(limit);
    }
    private void displayToat(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

