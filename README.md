# Famous People

Android app napravljena za ORWIMA-LV5.

U odnosu na početni zadatak, kombinirao sam relativni i linearni layout te dodao scroll view kako bi se mogli doći do dna (nisu svi mobiteli istih dimenzija). Aplikaciju sam testirao na Xiaomi Redmi Note 8T (fizički uređaj) te na Google Pixel 4 (emulator, API 29). 

## Screenshot:

![screenshot](/uploads/81c69594836a41e164dd98a7b367e3a2/screenshot.PNG)
